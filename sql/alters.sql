CREATE OR REPLACE TABLE hrdos_locations (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  UNIQUE KEY location_name (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 

INSERT INTO hrdos_locations (name) values ('Helsinki');
INSERT INTO hrdos_locations (name) values ('Tampere');
INSERT INTO hrdos_locations (name) values ('Turku');
INSERT INTO hrdos_locations (name) values ('Lahti');
INSERT INTO hrdos_locations (name) values ('Kouvola');
INSERT INTO hrdos_locations (name) values ('Jyv�skyl�');
INSERT INTO hrdos_locations (name) values ('Pori');
INSERT INTO hrdos_locations (name) values ('Kotka');
INSERT INTO hrdos_locations (name) values ('Joensuu');
INSERT INTO hrdos_locations (name) values ('Sein�joki');
INSERT INTO hrdos_locations (name) values ('Oulu');
INSERT INTO hrdos_locations (name) values ('Rovaniemi');
ALTER TABLE hrdos_events ADD COLUMN location INT(11) UNSIGNED;
UPDATE hrdos_events SET location=1;
ALTER TABLE hrdos_events MODIFY COLUMN location INT(11) UNSIGNED NOT NULL;
ALTER TABLE hrdos_events ADD FOREIGN KEY event_location (location) REFERENCES hrdos_locations (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE hrdos_events ADD COLUMN creator INT(11) UNSIGNED;
UPDATE hrdos_events SET creator=1;
ALTER TABLE hrdos_events MODIFY COLUMN creator INT(11) UNSIGNED NOT NULL;
ALTER TABLE hrdos_events ADD FOREIGN KEY event_creator (creator) REFERENCES hrdos_users (id) ON DELETE NO ACTION ON UPDATE NO ACTION;



ALTER TABLE hrdos_users ADD COLUMN last_location_id INT(11) UNSIGNED NOT NULL DEFAULT 1;
ALTER TABLE hrdos_users ADD FOREIGN KEY user_location (last_location_id) REFERENCES hrdos_locations (id) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE hrdos_events ADD COLUMN description TEXT;