<?php 

include "common.php";

function param($name) {
	global $_GET;
	
	if (isset($_GET[$name])) {
		return $_GET[$name];
	} else {
		http_response_code(400);
		die("Missing parameter: " . $name); 
	}
}

function paramOption($name) {
	global $_GET;
	
	if (isset($_GET[$name])) {
		return $_GET[$name];
	} else {
		return "";
	}
}

function paramArray($name) {
	global $_GET;
	
	if (isset($_GET[$name])) {
		return explode(",",$_GET[$name]);
	} else {
		http_response_code(400);
		die("Missing parameter: " . $name); 
	}
}

function authorized($id, $fbId, $role) {
	$sql = "SELECT 1 FROM hrdos_users WHERE id = ? AND fb_id = ? AND role >= ?";
	
	$data = getData($sql, array($id, $fbId, $role), "isi");
	
	$ok = (sizeof($data) > 0);
	if (!$ok) http_response_code(403);	
	return $ok;
}

function requestBody() {
	$inputJSON = file_get_contents('php://input');
	return json_decode( $inputJSON, TRUE );
}

function getEvents() {
	$sql = "SELECT e.id, e.name, e.venue, e.start_date, e.end_date, (e.start_date > DATE_ADD(NOW(), INTERVAL 14 DAY)) AS future, e.creator, l.id AS location_id, e.description, l.name AS location, (SELECT COUNT(DISTINCT user_id) FROM hrdos_signups s, hrdos_signup_roles r WHERE r.signup_id = s.id AND s.`status` = 1 AND s.event_id = e.id AND r.role IN ('HR','IPR','JR','OPR') AND r.`status` = 2 AND (SELECT COUNT(1) FROM hrdos_signup_roles r2 WHERE r2.signup_id = s.id AND r2.role IN ('JT','PBM','PBT','SK','PT','LT','IWB','SBO') AND r2.`status` = 2) = 0) refs, (SELECT COUNT(DISTINCT user_id) FROM hrdos_signups s, hrdos_signup_roles r WHERE r.signup_id = s.id AND s.`status` = 1 AND s.event_id = e.id AND r.role IN ('JT','PBM','PBT','SK','PT','LT','IWB','SBO') AND r.`status` = 2 AND (SELECT COUNT(1) FROM hrdos_signup_roles r2 WHERE r2.signup_id = s.id AND r2.role IN ('HR','IPR','JR','OPR') AND r2.`status` = 2) = 0) nsos, (SELECT COUNT(DISTINCT user_id) FROM hrdos_signups s, hrdos_signup_roles r WHERE r.signup_id = s.id AND s.`status` = 1 AND s.event_id = e.id AND r.role IN ('JT','PBM','PBT','SK','PT','LT','IWB','SBO') AND r.`status` = 2 AND (SELECT COUNT(1) FROM hrdos_signup_roles r2 WHERE r2.signup_id = s.id AND r2.role IN ('HR','IPR','JR','OPR') AND r2.`status` = 2) > 0) refnsos, (SELECT COUNT(DISTINCT user_id) FROM hrdos_signups s WHERE s.`status` = 1 AND s.event_id = e.id AND NOT EXISTS (SELECT 1 FROM hrdos_signup_roles r WHERE r.signup_id = s.id AND r.`status` > 0)) nones FROM hrdos_events e, hrdos_locations l WHERE start_date > DATE_SUB(NOW(), INTERVAL 1 DAY) AND e.location = l.id ORDER BY location ASC, start_date ASC";
	
	return getData($sql);
}

function getLocations() {
	$sql = "SELECT id, name FROM hrdos_locations";
	
	return getData($sql);
}

function getRoster($eventId) {
	$sql = "SELECT u.id, u.name, s.status AS in_event, s.comment, sr.role, sr.status
	        FROM hrdos_users u, hrdos_signups s LEFT OUTER JOIN hrdos_signup_roles sr ON s.id = sr.signup_id
			WHERE s.event_id = ? AND u.id = s.user_id
			ORDER BY u.id, sr.role";
	
	$data = getData($sql, array($eventId), "i");
	
	$result = array();
	foreach ($data as $item) {
		$key = $item["id"];
		if (!isset($result[$key])) {
			$result[$key] = array("id" => $item["id"], "name" => $item["name"], "inEvent" => $item["in_event"], "comment" => $item["comment"], "roles" => array("not" => array(), "maybe" => array(), "yes" => array()));
		}
		if ($item["status"] == 0) $result[$key]["roles"]["not"][] = $item["role"];
		else if ($item["status"] == 1) $result[$key]["roles"]["maybe"][] = $item["role"];
		else if ($item["status"] == 2) $result[$key]["roles"]["yes"][] = $item["role"];
	}
	
	$return = array();
	foreach ($result as $key => $value) {
		$return[] = $value;
	}
	return $return;

}
function getId($fbId, $fbName) {
	$sql = "SELECT u.id, u.role, u.last_location_id FROM hrdos_users u WHERE u.fb_id = ?";
	
	$idData = getData($sql, array($fbId), "s");
	
	if (sizeof($idData) == 0) {
		$sql = "INSERT INTO hrdos_users(fb_id, name) VALUES (?,?)";
		getData($sql, array($fbId, $fbName), "ss");
		
		$sql = "SELECT u.id, u.role FROM hrdos_users u WHERE u.fb_id = ?";	
		$idData = getData($sql, array($fbId), "s");
	}
	
	return $idData[0];
}

function setName($fbId, $userId, $name) {
    if (!authorized($userId, $fbId, 0)) return "";
	if (strlen($name) == 0) return "";
	
	$sql = "UPDATE hrdos_users SET display_name = ? WHERE fb_id = ?";
	
	getData($sql, array($name, $fbId), "ss");	
}

function setLocation($fbId, $userId, $locationId) {
    if (!authorized($userId, $fbId, 0)) return "";
	if (strlen($locationId) == 0) return "";
	
	$sql = "UPDATE hrdos_users SET last_location_id = ? WHERE fb_id = ?";
	
	getData($sql, array($locationId, $fbId), "is");	
}

function setRoleSignup($fbId, $eventId, $userId, $roles, $status) {
    if (!authorized($userId, $fbId, 0)) return "";
	
	$sql = "SELECT id FROM hrdos_signups WHERE user_id = ? AND event_id = ?";
	
	$signupId = getData($sql, array($userId, $eventId), "ii");

	if (sizeof($signupId) == 0) {
		$sql = "INSERT INTO hrdos_signups(user_id, event_id) VALUES (?,?)";
		getData($sql, array($userId, $eventId), "ii");
		$sql = "SELECT id FROM hrdos_signups WHERE user_id = ? AND event_id = ?";
		$signupId = getData($sql, array($userId, $eventId), "ii");
	}
	
	$signupId = $signupId[0]["id"];
	$sql = "INSERT INTO hrdos_signup_roles(signup_id, role, status) VALUES " . implode(", ", array_fill(0, sizeof($roles), "(?, ?, ?)"))
		 . " ON DUPLICATE KEY UPDATE status = VALUES(status)";
			
	$params = array();
	foreach ($roles as $role) {
		$params = array_merge($params, array($signupId, $role, $status));
	}
	
	getData($sql, $params, str_repeat("iss", sizeof($roles)));
}

function setSignup($fbId, $eventId, $userId, $status) {	
    if (!authorized($userId, $fbId, 0)) return "";

	$sql = "INSERT INTO hrdos_signups(user_id, event_id, status) VALUES (?,?,?)"
		 . " ON DUPLICATE KEY UPDATE status = VALUES(status)";
	getData($sql, array($userId, $eventId, $status), "iii");
}

function saveComment($fbId, $eventId, $userId, $comment) {	
    if (!authorized($userId, $fbId, 0)) return "";

	$sql = "UPDATE hrdos_signups SET comment = ? WHERE user_id = ? AND event_id = ?";
	getData($sql, array($comment, $userId, $eventId), "sii");
}

function saveEvent($fbId, $userId, $name, $venue, $location, $startTime, $endTime, $description, $eventId) {
	if (!authorized($userId, $fbId, 1)) return "";
	
	if (strlen($eventId) == 0) {
		$sql = "INSERT INTO hrdos_events (name, venue, start_date, end_date, creator, location, description) VALUES (?, ?, ?, ?, ?, ?, ?)";
		getData($sql, array($name, $venue, $startTime, $endTime, $userId, $location, $description), "ssssiis");
	} else {
		$sql = "UPDATE hrdos_events SET name = ?, venue = ?, start_date = ?, end_date = ?, creator = ?, location = ?, description = ? WHERE id = ?";
		getData($sql, array($name, $venue, $startTime, $endTime, $userId, $location, $description, $eventId), "ssssiisi");
	}
}

function deleteEvent($fbId, $userId, $eventId) {
	if (!authorized($userId, $fbId, 1)) return "";
	
	$sql = "DELETE FROM hrdos_events WHERE id = ? AND creator = ?";
	getData($sql, array($eventId, $userId), "ii");
}


$op = param("op");
$response = "";

switch ($op) {
	case "events":
		$response = getEvents();
		break;
	case "roster":
		$response = getRoster(param("eventId"));
		break;
	case "id":
		$response = getId(param("fbId"), param("fbName"));
		break;
	case "name":
		$response = setName(param("fbId"), param("name"));
		break;
	case "roleSignup":
		$response = setRoleSignup(param("fbId"), param("eventId"), param("userId"), paramArray("role"), param("status"));
		break;
	case "signup":
		$response = setSignup(param("fbId"), param("eventId"), param("userId"), param("status"));
		break;
	case "setLocation":
		$response = setLocation(param("fbId"), param("userId"), param("locationId"));
		break;
	case "saveComment":
		$response = saveComment(param("fbId"), param("eventId"), param("userId"), param("comment"));
		break;
	case "saveEvent":
		$response = saveEvent(param("fbId"), param("userId"), param("name"), param("venue"), param("locationId"), param("startTime"), param("endTime"), param("description"), paramOption("eventId"));
		break;
	case "deleteEvent":
		$response = deleteEvent(param("fbId"), param("userId"), param("eventId"));
		break;
	case "locations":
		$response = getLocations();
		break;
}

if ($response != "") echo json_encode($response);

?>
