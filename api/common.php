<?php 

include "config.php";

function getConnection() {
	$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT);
	
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	
	if (!$conn->set_charset("utf8")) {
		printf("Error loading character set utf8: %s\n", $conn->error);
	}
	
	return $conn;
}

function bind_array($stmt, &$row) {
    $md = $stmt->result_metadata();
	
	if (!$md) return;
	
    $params = array();
    while($field = $md->fetch_field()) {
        $params[] = &$row[$field->name];
    }

    call_user_func_array(array($stmt, 'bind_result'), $params);
}

function getData($sql, $params = array(), $paramtypes = "") {
	$conn = getConnection();
	
	$statement = $conn->prepare($sql);
	//error_log($sql . json_encode($params) . json_encode($paramtypes));
	if (count($params) > 0) {
		$queryParams = array();
		$queryParams[] = $paramtypes;
		
		foreach ($params as $id => $term) $queryParams[] = &$params[$id];
		
		call_user_func_array(array($statement, 'bind_param'), $queryParams);
	}
	
	$data = array();    
	$statement->execute();
	
	if (mysqli_connect_errno()) {
		error_log("MySQL error: %s\n", mysqli_connect_error());
	}
	
	do {
		$row = array();
		bind_array($statement, $row);        
		$data[] = $row; 
	} while ($next = $statement->fetch());

	// Remove extra null row
	array_pop($data);

	$conn->close();
	
	return $data;
}

function utf8_encode_deep(&$input) {
	if (is_string($input)) {
			$input = utf8_encode($input);
	} else if (is_array($input)) {
		foreach ($input as &$value) {
			utf8_encode_deep($value);
		}

		unset($value);
	} else if (is_object($input)) {
		$vars = array_keys(get_object_vars($input));
		foreach ($vars as $var) {
			utf8_encode_deep($input->$var);
		}
	}
}

?>
