import React from 'react';
import _ from 'lodash';

import { REF_ROLES, NSO_ROLES } from './util.js';

import {OverlayTrigger, Badge, Button, Popover} from 'react-bootstrap';

export default class General extends React.Component {
    constructor(props) {
        super(props);
    }
    getExampleCrewDynamic() {
        var priority = ['HR', 'IPR', 'JR', 'JT', 'JR', 'PBM', 'PBT', 'PBT', 'OPR', 'SK', 'SK', 'OPR', 'OPR', 'PT', 'LT', 'LT', 'IWB', 'SBO'];
        var roster = _.cloneDeep(this.props.roster);
        var result = priority.map(role => ({role: role, name: ''}));
        priority.forEach(role => {result[role] = '';});

        var assignRoles = function(roleProperty) {
            try {
                while (roster.length > 0 && priority.length > 0) {
                // The candidate with fewest remaining roles
                    var remainingRoleAmounts = roster.map(user => _.filter(user.roles[roleProperty], role => _.contains(priority, role)).length);
                    remainingRoleAmounts = _.filter(remainingRoleAmounts, amount => amount > 0);
                    if (remainingRoleAmounts.length == 0) break;
                    var fewestRemainingRoles = _.min(remainingRoleAmounts);
                    var candidate = _.find(roster, user => _.filter(user.roles[roleProperty], role => _.contains(priority, role)).length === fewestRemainingRoles);

                    // The most important role that candidate has
                    var remainingRoles = _.filter(candidate.roles[roleProperty], role => _.contains(priority, role));
                    var role = _.dropWhile(priority, role => !_.contains(remainingRoles, role))[0];
                    // Fill role, remove candidate
                    var roleObject = _.find(result, _.matches({role: role, name: ''}));
                    roleObject.name = candidate.name;
                    roleObject.maybe = (roleProperty === 'maybe');

                    roster = _.without(roster, candidate);
                    priority.splice(priority.indexOf(role), 1);
                }
            }
            catch (err) {
                console.log(err);
            }
        };

        assignRoles('yes');
        assignRoles('maybe');

        return result;
    }
    getExampleCrewGreedy() {
        var priority = ['HR', 'IPR', 'JR', 'JT', 'JR', 'PBM', 'PBT', 'PBT', 'OPR', 'SK', 'SK', 'OPR', 'OPR', 'PT', 'LT', 'LT', 'IWB', 'SBO'];
        var roster = _.cloneDeep(this.props.roster);
        var result = priority.map(function(role) {return {role: role, name: ''};});
        priority.forEach(function(role) {result[role] = '';});

        try {
            for (var x = 0 ; x < priority.length ; x++) {
                var role = priority[x];
                var maybe = false;
                var candidates = _.filter(roster, user => _.contains(user.roles.yes, role));
                if (candidates.length == 0) { maybe = true; candidates = _.filter(roster, user => _.contains(user.roles.maybe, role)); }
                if (candidates.length == 0) continue;
                var leastRemainingRoles = _.min(candidates.map(user => _.filter(user.roles.yes, role => _.contains(priority, role)).length));
                var candidate = _.find(candidates, user => _.filter(user.roles.yes, role => _.contains(priority, role)).length === leastRemainingRoles);

                var roleObject = _.find(result, _.matches({role: role, name: ''}));
                roleObject.name = candidate.name;
                roleObject.maybe = maybe;
                roster = _.without(roster, candidate);
            }
        }
        catch (err) {
            console.log(err);
        }

        return result;
    }
    formatCrewPart(crew, positions) {
        var rends = _.filter(crew, e => _.contains(positions, e.role));
        var i = 0;
        var elems = rends.map(
            rend => (<a href="#" key={++i} className={'btn btn-' + ((rend.name === '') ? 'default' : ((rend.maybe) ? 'warning' : 'success'))} role="button">{rend.role}<br/>{rend.name}</a>)
        );
        return (<div className="btn-group btn-group-justified" role="group" aria-label="ref-roster">
            {elems}
        </div>);
    }
    render() {
        var roster = this.props.roster.map(function (user) {
            var yesRoles = user.roles.yes.map(function (rosterRole) {
                return (<span key={rosterRole} className={'badge ' + ((REF_ROLES.indexOf(rosterRole) == -1) ? 'badge-nso' : 'badge-ref')}>{rosterRole}</span>);
            });
            var maybeRoles = user.roles.maybe.map(function (rosterRole) {
                return (<span key={rosterRole} className={'badge badge-maybe ' + ((REF_ROLES.indexOf(rosterRole) == -1) ? 'badge-nso' : 'badge-ref')}>{rosterRole}</span>);
            });

            var comment = '';
            if (user.comment && user.comment.length > 0) {
                comment =
          <OverlayTrigger trigger="click" rootClose={true} placement="top" overlay={<Popover id={`user_${user.id}_comment`}>{user.comment}</Popover>}>
              <Button><Badge>?</Badge></Button>
          </OverlayTrigger>;
            }

            return (
                <tr key={user.id}>
                    <td>{user.name} {comment}</td>
                    <td>
                        {yesRoles}
                    </td>
                    <td>
                        {maybeRoles}
                    </td>
                </tr>
            );}.bind(this));

        var inactiveRoster = this.props.inactiveRoster.map(function (user) {
            return (<tr key={user.id}><td>{user.name}</td></tr>);
        });

        var exampleCrew = this.getExampleCrewDynamic();
        var refLine = this.formatCrewPart(exampleCrew, ['HR', 'IPR', 'JR', 'OPR']);
        var nsoLine1 = this.formatCrewPart(exampleCrew, ['JT', 'PT', 'IWB', 'PBM', 'PBT']);
        var nsoLine2 = this.formatCrewPart(exampleCrew, ['LT', 'SK', 'SBO']);

        var exampleCrewG = this.getExampleCrewGreedy();
        var refLineG = this.formatCrewPart(exampleCrewG, ['HR', 'IPR', 'JR', 'OPR']);
        var nsoLine1G = this.formatCrewPart(exampleCrewG, ['JT', 'PT', 'IWB', 'PBM', 'PBT']);
        var nsoLine2G = this.formatCrewPart(exampleCrewG, ['LT', 'SK', 'SBO']);


        return (

            <div>
                <h2 className="text-center">Roster</h2>
                <hr/>
                <h3>Signups</h3>
                <table className="table">
                    <thead>
                        <tr><th>Name</th><th>Yes</th><th>Maybe</th></tr>
                    </thead>
                    <tbody>
                        {roster}
                    </tbody>
                </table>

                <hr/>
                <h3>Example crew compositions</h3>
                <h4>Greedy</h4>
                {refLineG}
                {nsoLine1G}
                {nsoLine2G}
                <h4>Dynamic</h4>
                {refLine}
                {nsoLine1}
                {nsoLine2}
                <h3>Not coming</h3>
                <table className="table">
                    <tbody>
                        {inactiveRoster}
                    </tbody>
                </table>
            </div>
        );
    }
}
