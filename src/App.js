import React from 'react'
import {render} from 'react-dom'

import { HashRouter, Route, Link } from 'react-router-dom'

import Auth from './Auth.jsx'

render(
  <HashRouter>
    <div>
        <Route path="/" component={Auth} />
        <Route path="/event/:eventId" component={Auth} />
    </div>
  </HashRouter>, document.getElementById('root'))
