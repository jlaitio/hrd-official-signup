import React from 'react';
import $ from 'jquery';
import _ from 'lodash';

import Events from './Events.jsx';
import Roles from './Roles.jsx';
import General from './General.jsx';
import Admin from './Admin.jsx';
import { REF_ROLES, NSO_ROLES } from './util.js';

import {Row, Col, Navbar, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';

export default class Base extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [],
            locations: [],
            eventId: '',
            roster: [],
            inactiveRoster: [],
            inEvent: undefined,
            comment: null,
            saveSuccessVisible: false,
            showProfile: false
        };
        this.apiUrl =  process.env.API_URL;
    }
    componentWillMount() {
        this.updateEvents(this.props.eventId);
        this.updateLocations();
    }
    ajax(props, callback, showLoadAnimation = true) {
        this.setState({loading: showLoadAnimation});

        var doneFunction = (data => {
            callback(data);
            this.setState({loading: false});
        });
        $.ajax(props).done(doneFunction).fail(() => this.setState({loading: false}));
    }
    updateEvents(initialEventId) {
        this.ajax({
            url: this.apiUrl + '?op=events',
            type: 'GET',
            dataType: 'json',
            cache: false
        },
            (data => {
                this.setState({events: data});
                if (initialEventId && initialEventId.length > 0) this.loadRoster(initialEventId);
            }));
    }
    updateLocations() {
        this.ajax({
            url: this.apiUrl + '?op=locations',
            type: 'GET',
            dataType: 'json',
            cache: false
        },
            (data => {
                this.setState({locations: data});
            }));
    }
    signup(status) {
        this.ajax({
            url: this.apiUrl + '?op=signup&fbId='+this.props.user.id+'&eventId='+this.state.eventId+'&userId='+this.props.id+'&status='+status,
            type: 'GET',
            cache: false
        },
            ((roster) => {
                this.handleRosterChange(this.state.eventId, roster);
            }));
    }
    roleSignup(role, status) {
        if (role === 'ALL_ref') role = REF_ROLES.join(',');
        else if (role === 'ALL_nso') role = NSO_ROLES.join(',');

        var iStatus;
        if (status === 'no') iStatus = 0;
        else if (status === 'maybe') iStatus = 1;
        else if (status === 'yes') iStatus = 2;

        this.ajax({
            url: this.apiUrl + '?op=roleSignup&fbId='+this.props.user.id+'&eventId='+this.state.eventId+'&userId='+this.props.id+'&role='+role+'&status='+iStatus,
            type: 'GET',
            cache: false
        },
            ((roster) => {
                this.handleRosterChange(this.state.eventId, roster);
            }));
    }
    setLocation(location) {
        this.ajax({
            url: this.apiUrl + '?op=setLocation&fbId='+this.props.user.id+'&location='+location+'&userId='+this.props.id,
            type: 'GET',
            cache: false
        },
            ((data) => {

            }));
    }
    saveComment(comment) {
        this.ajax({
            url: this.apiUrl + '?op=saveComment&fbId='+this.props.user.id+'&eventId='+this.state.eventId+'&userId='+this.props.id+'&comment='+comment,
            type: 'GET',
            cache: false
        },
            ((roster) => {
                this.handleRosterChange(this.state.eventId, roster, true);
            }));
    }
    saveEvent(name, venue, startTime, endTime, location, description, eventId) {
        this.ajax({
            url: this.apiUrl + '?op=saveEvent',
            data: {startTime: startTime, endTime: endTime, name: name, venue: venue, fbId: this.props.user.id, userId: this.props.id, location: location, description: description, eventId: eventId},
            type: 'GET',
            cache: false
        },
            ((data) => { this.updateEvents(); }));
    }
    deleteEvent(id) {
        if (id == this.state.eventId) this.setState({eventId: ''});
        this.ajax({
            url: this.apiUrl + '?op=deleteEvent',
            data: {eventId: id, fbId: this.props.user.id, userId: this.props.id},
            type: 'GET',
            cache: false
        },
            ((data) => { this.updateEvents(); }));
    }
    toggleSaveSuccessVisible() {
        this.setState({saveSuccessVisible: !this.state.saveSuccessVisible});
    }
    loadRoster(eventId) {
        eventId = typeof eventId !== 'undefined' ? eventId : this.state.eventId;
        this.ajax({
            url: this.apiUrl + '?op=roster&eventId='+eventId,
            type: 'GET',
            dataType: 'json',
            cache: false
        },
            ((roster) => {
                this.handleRosterChange(eventId, roster);
            }));
    }
    handleRosterChange(eventId, roster, commentChange) {
        var me = _.find(roster, _.matchesProperty('id', this.props.id));

        var inEvent;
        if (me && me.inEvent && me.inEvent == 1) inEvent = true;
        else if (me && me.inEvent == 0) inEvent = false;
        else inEvent = undefined;

        var comment = null;
        if (me && me.comment && me.comment.length > 0) comment = me.comment;

        const rosterParts = _.partition(roster, function(user) {return user.inEvent == 1;});

        this.setState({eventId: eventId, inEvent: inEvent, comment: comment, roster: rosterParts[0], inactiveRoster: rosterParts[1], saveSuccessVisible: commentChange ? true : false});
        $('#roleElement')[0].scrollIntoView({behavior: 'smooth'});
    }
    toggleNameChangeDialog() {

    }
    changeName(name) {
        this.ajax({
            url: this.apiUrl + '?op=name&fbId='+this.props.user.id+'&userId='+this.props.id+'&name='+name,
            type: 'GET',
            cache: false
        },
            ((roster) => {
                this.handleRosterChange(this.state.eventId, roster, true);
            }));
    }
    render() {
        var roles;
        var rolesElement;
        var generalElement;
        var adminElement;
        var spinner;

        var me = _.find(this.state.roster, _.matchesProperty('id', this.props.id));
        if (me === undefined) roles = {not: [], maybe: [], yes: []};
        else roles = me.roles;

        if (this.state.eventId) {
            rolesElement =
      <Roles
          roles={roles}
          signup={this.signup.bind(this)}
          roleSignup={this.roleSignup.bind(this)}
          events={this.state.events}
          eventId={this.state.eventId}
          inEvent={this.state.inEvent}
          comment={this.state.comment}
          saveComment={this.saveComment.bind(this)}
          saveSuccessVisible={this.state.saveSuccessVisible}
          toggleSaveSuccessVisible={this.toggleSaveSuccessVisible.bind(this)} />;

            generalElement = <General roster={this.state.roster} inactiveRoster={this.state.inactiveRoster} />;
        }

        if (this.state.loading) {
            spinner =
    <div className="centered sk-cube-grid">
        <div className="sk-cube sk-cube1"></div>
        <div className="sk-cube sk-cube2"></div>
        <div className="sk-cube sk-cube3"></div>
        <div className="sk-cube sk-cube4"></div>
        <div className="sk-cube sk-cube5"></div>
        <div className="sk-cube sk-cube6"></div>
        <div className="sk-cube sk-cube7"></div>
        <div className="sk-cube sk-cube8"></div>
        <div className="sk-cube sk-cube9"></div>
    </div>;
        }

        if (this.props.role > 0) adminElement = <Admin userId={this.props.id} events={this.state.events} locations={this.state.locations} saveEvent={this.saveEvent.bind(this)} deleteEvent={this.deleteEvent.bind(this)}/>;

        const navBar = <Navbar collapseOnSelect>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href="#">DerbyOfficialsDigitalOrganizer</a>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
        </Navbar>;

        /*const navBar = <Navbar collapseOnSelect fixedTop>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="#">DerbyOfficialsDigitalOrganizer</a>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav pullRight>
          <NavItem eventKey={1} href="#eventsElement">Events</NavItem>
          <NavItem eventKey={2} href="#">Change Name</NavItem>
        </Nav>
      </Navbar.Collapse>
    </Navbar>*/

        return (
            <div>
                {navBar}
                {spinner}
                <Col md={3}>
                    {adminElement}
                    <Events events={this.state.events} eventId={this.state.eventId} initialLocation={this.props.initialLocation} loadRoster={this.loadRoster.bind(this)} setLocation={this.setLocation.bind(this)}/>
                </Col>
                <Col md={4}>
                    {rolesElement}
                </Col>
                <Col md={5}>
                    {generalElement}
                </Col>
            </div>
        );
    }
}
