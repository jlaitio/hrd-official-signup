import React from 'react';
import moment from 'moment';
import _ from 'lodash';

import DateTimeField from 'react-bootstrap-datetimepicker';
import {Row, Col, Panel, Accordion, Input, Button} from 'react-bootstrap';
import {Table, Tr, Td} from 'reactable';

import EventAdminModal from './EventAdminModal.jsx';

export default class Admin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            activePanel: '0'
        };
        this.dateFormat = 'YYYY-MM-DD HH:mm:ss';
    }
    handlePanelChange(newPanel) {
        if (newPanel == this.state.activePanel) this.setState({activePanel: '0'});
        else if (+newPanel > 1) this.setState({activePanel: newPanel});
    }
    handleEventSave(date, start, end, name, venue, location, description, eventId) {
        const dateM = moment(date);
        const startM = moment(start);
        const endM = moment(end);

        startM.year(dateM.year());
        startM.month(dateM.month());
        startM.date(dateM.date());
        endM.year(dateM.year());
        endM.month(dateM.month());
        endM.date(dateM.date());

        const startF = startM.format(this.dateFormat);
        const endF = endM.format(this.dateFormat);
        this.props.saveEvent(name, venue, startF, endF, location, description, eventId);
    }
    createEvent() {
        this.setState({showModal: true, modalEvent: undefined});
    }
    editEvent(eventId) {
        this.setState({showModal: true, modalEvent: _.find(this.props.events, event => event.id === eventId)});
    }
    handleEventDelete(eventId) {
        this.props.deleteEvent(eventId);
    }
    closeModal() {
        this.setState({showModal: false});
    }
    render() {
        var newEventAddTitle = <h3 className="panel-title text-center">Add new event</h3>;
        var editEventListTitle = <h3 className="panel-title text-center">Edit your events</h3>;
        var eventListTitle = <h3 className="panel-title text-center">Delete your events</h3>;

        var eventModal;

        if (this.state.showModal && this.state.modalEvent) {
            const modalEventData = {
                name: this.state.modalEvent.name,
                venue: this.state.modalEvent.venue,
                date: this.state.modalEvent.start_date,
                start: this.state.modalEvent.start_date,
                end: this.state.modalEvent.end_date,
                location: this.state.modalEvent.location,
                description: this.state.modalEvent.description
            };

            eventModal = <EventAdminModal locations={this.props.locations} eventData={modalEventData} eventId={this.state.modalEvent.id} save={this.handleEventSave.bind(this)} close={this.closeModal.bind(this)} />;

        } else if (this.state.showModal) {
            const modalEventData = {
                name: '',
                venue: '',
                date: moment().day(5).format(this.dateFormat),
                start: moment().hour(18).minute(0).second(0).format(this.dateFormat),
                end: moment().hour(22).minute(0).second(0).format(this.dateFormat),
                location: 'Helsinki',
                description: ''
            };
            eventModal = <EventAdminModal locations={this.props.locations} eventData={modalEventData} save={this.handleEventSave.bind(this)} close={this.closeModal.bind(this)}  />;
        }

        return (
            <div>
                <h2 className="text-center">Administration</h2>
                <hr/>
                {eventModal}
                <Accordion activeKey={this.state.activePanel} onSelect={this.handlePanelChange.bind(this)}>
                    <Panel eventKey="1" bsStyle="primary" header={newEventAddTitle} onClick={this.createEvent.bind(this)} collapsible={false}/>
                    <Panel eventKey="2" bsStyle="primary" header={editEventListTitle}>
                        <Table sortable className="table-striped table-condensed table-bordered table-hover" columns={[{key: 'name', label: 'Name'},{key: 'date', label: 'Date'},{key: 'edit', label: ''}]}>
                            {this.props.events.filter(event => (event.creator == this.props.userId)).map(event => (
                                <Tr key={event.id}>
                                    <Td column="name">{event.name}</Td>
                                    <Td column="date">{event.start_date}</Td>
                                    <Td column="edit"><Button bsStyle="primary" onClick={this.editEvent.bind(this, event.id)}>Edit</Button></Td>
                                </Tr>)
                            )}
                        </Table>
                    </Panel>
                    <Panel eventKey="3" bsStyle="primary" header={eventListTitle}>
                        <Table sortable className="table-striped table-condensed table-bordered table-hover" columns={[{key: 'name', label: 'Name'},{key: 'date', label: 'Date'},{key: 'del', label: ''}]}>
                            {this.props.events.filter(event => (event.creator == this.props.userId)).map(event => (
                                <Tr key={event.id}>
                                    <Td column="name">{event.name}</Td>
                                    <Td column="date">{event.start_date}</Td>
                                    <Td column="del"><Button bsStyle="primary" onClick={this.handleEventDelete.bind(this, event.id)}>Delete</Button></Td>
                                </Tr>)
                            )}
                        </Table>
                    </Panel>
                </Accordion>
                <hr/>
            </div>
        );
    }
}
