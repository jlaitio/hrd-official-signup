import React from 'react';
import moment from 'moment';
import $ from 'jquery';
import _ from 'lodash';

import {Badge, Button, ButtonGroup, Accordion, Panel, PanelGroup} from 'react-bootstrap';

moment.updateLocale('en', {
    calendar : {
        lastDay : '[Yesterday at] HH:mm',
        sameDay : '[Today at] HH:mm',
        nextDay : '[Tomorrow at] HH:mm',
        lastWeek : '[last] dddd [at] HH:mm',
        nextWeek : 'dddd [at] HH:mm',
        sameElse : 'ddd DD.MM HH:mm'
    }
});

export default class Events extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        $('a.btn').click( function(event) {
            event.preventDefault();
        });
    }
    componentDidUpdate() {
        $('a.btn').click( function(event) {
            event.preventDefault();
        });
    }
    render() {
        var events = this.props.events;
    
        var eventsByLocation = _.groupBy(events, event => event.location);
        var locations = _.keys(eventsByLocation).sort();
        var content = locations.map(location => {
            var header = <span>{location} <Badge className="pull-right">{eventsByLocation[location].length}</Badge></span>;
            return (
                <Panel key={location} eventKey={location} header={header} onClick={this.props.setLocation.bind(null, location)}>
                    <ButtonGroup vertical block>
                        {eventsByLocation[location].map(event => {
                            var eventBadges;
                            if (event.refnsos == 0 && event.nsos == 0 && event.refs == 0 && event.nones > 0) {
                                eventBadges =
             <Badge className="pull-right badge-none">{event.nones}</Badge>;
                            } else {
                                eventBadges =
                                <div>
                                    <Badge className="pull-right badge-refnso">{event.refnsos}</Badge>
                                    <Badge className="pull-right badge-nso">{event.nsos}</Badge>
                                    <Badge className="pull-right badge-ref">{event.refs}</Badge>
                                </div>;
                            }

                            return (
                                <Button key={event.id} onClick={this.props.loadRoster.bind(null, event.id)}>
                                    <span className="pull-left">
                                        <strong>{event.name}</strong>
                                    </span>
                                    <span className="pull-right">
                                        {moment(event.start_date).calendar()}
                                        <br/>
                                        {eventBadges}
                                    </span>
                                </Button>);
                        })}
                    </ButtonGroup>
                </Panel>);
        });
        return (
            <div id="eventsElement">
                <h2 className="text-center">Open signups</h2>
                <hr/>
                <div className="text-center">
                    <PanelGroup defaultActiveKey={this.props.initialLocation} accordion>
                        {content}
                    </PanelGroup>
                </div>
            </div>
        );
    }
}
