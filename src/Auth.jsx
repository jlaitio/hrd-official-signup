import React from 'react';
import $ from 'jquery';

import Base from './Base.jsx';

export default class Auth extends React.Component {
    constructor() {
        super();
        this.state = {
            authenticated: false,
            id: 0,
            role: 0,
            initialLocation: 0,
            user: {}
        };
    }
    componentDidMount() {
        FB.init({
            appId      : process.env.FB_APP_ID,
            cookie     : true,
            xfbml      : true,
            version    : 'v2.10'
        });

        this.checkLoginState();
    }
    statusChange(response) {
        var self = this;

        if (response.status === 'connected') {
            FB.api('/me', function(response) {
                //console.log('login info: ' + JSON.stringify(response))
                $.ajax({
                    url: process.env.API_URL + '?op=id&fbId='+response.id+'&fbName='+response.name,
                }).done(function(data) {
                    $('#loginStatus').text('Logged in successfully, stand by.');

                    self.setState({authenticated: true, id: data.id, role: data.role, initialLocation: data.location, user: response});
                }).fail(function(data) {
                    $('#loginStatus').text('Problem with login: ' + JSON.stringify(data));
                });
            });
        } else if (response.status === 'not_authorized') {
            this.setState({loginStatus: 'You have not yet authorized this app in Facebook. Please log in.'});
        } else {
            this.setState({loginStatus: <div>You have not logged into facebook. Please log in. If the login button doesn't work, please log in at <a href="http://www.facebook.com">http://www.facebook.com</a> and then load this page again.</div>});
        }
    }
    checkLoginState() {
        var self = this;
        $('#loginStatus').text('Checking your login state...');
        FB.getLoginStatus(function(response) {
            self.statusChange(response);
        });
    }
    render() {
        var content;
        if (this.state.authenticated) {
            content = <Base id={this.state.id} role={this.state.role} initialLocation={this.state.initialLocation} eventId={this.props.match.params.eventId} user={this.state.user}/>;
        }
        else {
            content = <div><div className="fb-login-button" data-size="xlarge" scope="public_profile"></div><div id="loginStatus">{this.state.loginStatus}</div></div>;
        }

        return (
            <div>
                {content}
            </div>
        );
    }
}
