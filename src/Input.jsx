import React from 'react';

import {Modal, Row, Col, ControlLabel, FormControl, FormGroup, HelpBlock} from 'react-bootstrap';

export default class Input extends React.Component {
    constructor(props) {
        super(props);
        this.getValidationState = this.getValidationState.bind(this);
    }
    getValidationState() {
        if (this.props.requiredLength && (!this.props.value || this.props.value.length < this.props.requiredLength)) {
            return 'error';
        } else if (this.requiredLength && this.props.value && this.props.value.length > 0) {
            return 'success';
        }
    }
    render() {
        const fcProps = {
            componentClass: (this.props.type === 'textarea') ? 'textarea' : undefined,
            rows: (this.props.type === 'textarea') ? this.props.rows : undefined,
            type: this.props.type,
            value: this.props.value,
            placeholder: this.props.placeholder,
            onChange: this.props.onChange
        };
        return (

            <FormGroup
                controlId="input_field"
                validationState={this.getValidationState()}
            >
                <FormControl {...fcProps} />
                <FormControl.Feedback />
            </FormGroup>
        );
    }
}
