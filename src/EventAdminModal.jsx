import React from 'react';

import DateTimeField from 'react-bootstrap-datetimepicker';
import {Modal, Row, Col, Button} from 'react-bootstrap';

import Input from './Input.jsx';

export default class EventAdminModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            eventName: this.props.eventData.name,
            eventVenue: this.props.eventData.venue,
            eventDate: this.props.eventData.date,
            eventStart: this.props.eventData.start,
            eventEnd: this.props.eventData.end,
            eventLocation: this.props.eventData.location,
            eventDescription: this.props.eventData.description,
            eventId: this.props.eventId
        };
        this.dateFormat = 'YYYY-MM-DD HH:mm:ss';
    }
    componentWillMount() {

    }
    handleDate(field, value) {
        var newState = {};
        newState[field] = value;
        this.setState(newState);
    }
    handleInput(field, event) {
        var newState = {};
        newState[field] = event.target.value;
        this.setState(newState);
    }
    newEventValid() {
        var date = this.state.eventDate;
        var start = this.state.eventStart;
        var end = this.state.eventEnd;
        var name = this.state.eventName;
        var venue = this.state.eventVenue;
        return (date && start && end && name && name.length > 0 && venue && venue.length > 0);
    }
    save() {
        this.props.save(this.state.eventDate, this.state.eventStart, this.state.eventEnd, this.state.eventName,
            this.state.eventVenue, this.state.eventLocation, this.state.eventDescription, this.state.eventId);
        this.props.close();
    }
    render() {
        return (
            <Modal show={true} onHide={this.props.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Event information</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col md={3}>Name</Col>
                        <Col md={9}>
                            <Input type="text" requiredLength={1} value={this.state.eventName} placeholder="Event name" onChange={this.handleInput.bind(this, 'eventName')} />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>Venue</Col>
                        <Col md={9}>
                            <Input type="text" requiredLength={1} value={this.state.eventVenue} placeholder="Event venue" ref="eventVenue" onChange={this.handleInput.bind(this, 'eventVenue')} />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>Description</Col>
                        <Col md={9}>
                            <Input type="textarea" rows="8" value={this.state.eventDescription} placeholder="(schedule etc.)" ref="eventDescription" onChange={this.handleInput.bind(this, 'eventDescription')} />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>Date</Col>
                        <Col md={9}>
                            <DateTimeField inputFormat="ddd YYYY-MM-DD" mode="date" format={this.dateFormat} dateTime={this.state.eventDate} onChange={this.handleDate.bind(this, 'eventDate')} />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>Start</Col>
                        <Col md={9}>
                            <DateTimeField inputFormat="HH:mm" mode="time" format={this.dateFormat} dateTime={this.state.eventStart} onChange={this.handleDate.bind(this, 'eventStart')} />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>End</Col>
                        <Col md={9}>
                            <DateTimeField inputFormat="HH:mm" mode="time" format={this.dateFormat} dateTime={this.state.eventEnd} onChange={this.handleDate.bind(this, 'eventEnd')} />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>Location</Col>
                        <Col md={9}>
                            <Input type="select" value={this.state.eventLocation} ref="eventLocation" onChange={this.handleInput.bind(null, 'eventLocation')}>
                                {this.props.locations.map(location =>
                                    <option key={location} value={location}>{location}</option>
                                )}
                            </Input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={9} mdOffset={3}>
                            <Button block bsStyle="primary" onClick={this.save} disabled={!this.newEventValid()}>Save event</Button>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.close}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
