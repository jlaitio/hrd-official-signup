import React from 'react';
import $ from 'jquery';
import _ from 'lodash';

import {Alert, Button, ButtonGroup, Panel} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import { REF_ROLES, NSO_ROLES } from './util.js';

export default class Roles extends React.Component {
    constructor(props) {
        super(props);
        this.state = { descriptionOpen: false };
    }
    componentDidMount() {
        $('a.btn').click( function(event) {
            event.preventDefault();
        });
    }
    componentDidUpdate() {
        $('a.btn').click( function(event) {
            event.preventDefault();
        });
    }
    createRoles(roles, label, title) {
        var roleList = roles.map(function (roleName) {
            var props = [{bsStyle: 'default', href: '#'},{bsStyle: 'default', href: '#'},{bsStyle: 'default', href: '#'}];
            if (this.props.roles.yes.indexOf(roleName) != -1) { props[0].bsStyle = 'success'; props[0].active = true; }
            else if (this.props.roles.maybe.indexOf(roleName) != -1) { props[1].bsStyle = 'warning'; props[1].active = true; }
            else if (this.props.roles.not.indexOf(roleName) != -1) { props[2].bsStyle = 'danger'; props[2].active = true; }

            return (
                <ButtonGroup key={roleName} justified>
                    <Button {...props[0]} onClick={this.props.roleSignup.bind(null, roleName, 'yes')}>{roleName} (yes)</Button>
                    <Button {...props[1]} onClick={this.props.roleSignup.bind(null, roleName, 'maybe')}>{roleName} (maybe)</Button>
                    <Button {...props[2]} onClick={this.props.roleSignup.bind(null, roleName, 'no')}>{roleName} (no)</Button>
                </ButtonGroup>
            );}.bind(this));

        return (<div>
            <h4>{title}</h4>
            <ButtonGroup justified>
                <Button href="#" onClick={this.props.roleSignup.bind(null, 'ALL_'+label, 'yes')}>ALL (yes)</Button>
                <Button href="#" onClick={this.props.roleSignup.bind(null, 'ALL_'+label, 'maybe')}>ALL (maybe)</Button>
                <Button href="#" onClick={this.props.roleSignup.bind(null, 'ALL_'+label, 'no')}>ALL (no)</Button>
            </ButtonGroup>
            {roleList}
        </div>);
    }
    saveComment() {
        var comment = $('#event-signup-comment').val();
        this.props.saveComment(comment);
    }
    render() {
        var refRoles = '';
        var nsoRoles = '';
        var divider = '';
        var comment = '';
        var commentSaveSuccess = '';
        var eventDescriptionButton = '';
        var eventDescription = '';

        if (this.props.saveSuccessVisible) {
            commentSaveSuccess =
        <div>
            <Alert bsStyle='success' onDismiss={this.props.toggleSaveSuccessVisible} dismissAfter={1500}>
                <h4>Comment saved successfully</h4>
            </Alert>
        </div>;
        }

        if (this.props.inEvent) {
            refRoles = this.createRoles(REF_ROLES, 'ref', 'Referee');
            nsoRoles = this.createRoles(NSO_ROLES, 'nso', 'NSO');
            divider = <hr/>;
            comment =
        <div className="form-horizontal">
            <div className="form-group">
                <div className="col-sm-9">
                    <textarea rows="3" className="form-control" id="event-signup-comment" defaultValue={this.props.comment} placeholder="Optional comment (for example, can only work part of event)"/>
                </div>
                <div className="col-sm-3">
                    <button type="submit" className="btn btn-default" onClick={this.saveComment}>Save comment</button>
                </div>
            </div>
        </div>;
        }

        var eventId = this.props.eventId;
        var event = _.find(this.props.events, event => event.id === eventId);

        if (event.description && event.description.length > 0) {
            eventDescription =
            <Panel collapsible expanded={this.state.descriptionOpen}>
                <pre>{event.description}</pre>
            </Panel>;
            eventDescriptionButton =
            <Button onClick={ ()=> this.setState({ descriptionOpen: !this.state.descriptionOpen })}>
                Additional info
            </Button>;
        }

        return (
            <div id="roleElement">
                <h3 className="text-center">{event.name}</h3>
                <h4 className="text-center">at {event.venue}</h4>
                <h4 className="text-center">starting at {event.start_date}</h4>
                <h4 className="text-center">{eventDescriptionButton} <Button><Link to={'/event/'+this.props.eventId}>Link to this event</Link></Button></h4>
                {eventDescription}
                <hr/>
                <ButtonGroup justified>
                    <Button href="#" bsStyle={(this.props.inEvent) ? 'success' : 'default'} active={this.props.inEvent} onClick={this.props.signup.bind(null, 1)}>IN</Button>
                    <Button href="#" bsStyle={(this.props.inEvent !== false) ? 'default' : 'danger'} active={this.props.inEvent === false} onClick={this.props.signup.bind(null, 0)}>OUT</Button>
                </ButtonGroup>
                {divider}
                {commentSaveSuccess}
                {comment}
                {divider}
                <div className="text-center">
                    {refRoles}
                    <hr/>
                    {nsoRoles}
                </div>
            </div>
        );
    }
}
