const _ = require('lodash')
const axios = require('axios');
const usermap = require('./usermap.json')
const uuid = require('uuid/v1')

const creatorMap = {
    13: "62178652-7e69-11e7-bb31-be2e44b06b34",
    38: "6217a286-7e69-11e7-bb31-be2e44b06b34",
    45: "6217ac5e-7e69-11e7-bb31-be2e44b06b34",
    196: "621891fa-7e69-11e7-bb31-be2e44b06b34"
}

const nameToId = _.reduce(usermap, (obj,user)  => {
     obj[user.name] = user.id
     return obj;
}, {});

axios.get('http://signup.helsinkirollerderby.com/api/?op=events').then((response) => {

  const events = response.data

  var evs = []

  return axios.all(events.map((e) => (axios.get('http://signup.helsinkirollerderby.com/api/?op=roster&eventId='+e.id)).then((r) => [e, r.data] )))
})
.then((d) => {
    const data = d.map((ev) => {
        const roster = ev[1].map((r) => (Object.assign({}, r, {id: nameToId[r.name]})))
        const creator = (creatorMap[ev[0].creator]) ? creatorMap[ev[0].creator] : ev[0].creator

        return Object.assign({}, ev[0], {id: uuid(), refs: undefined, nsos: undefined, refnsos: undefined, nones: undefined, future: undefined, location_id: undefined, roster: roster, creator: creator})
    })
    console.log(JSON.stringify(data, null, 2))
})
.catch((e) => console.log(e))
