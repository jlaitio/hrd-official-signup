'use strict';

const moment = require('moment')
const _ = require('lodash')
const uuid = require('uuid/v1')
const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient()

module.exports.handler = (event, context, callback) => {

    const LOCATIONS = ["Helsinki", "Turku", "Tampere", "Kouvola", "Lahti", "Oulu", "Joensuu", "Jyväskylä", "Rovaniemi", "Seinäjoki", "Porvoo", "Kotka", "Pori"]
    const REF_ROLES = ['HR','IPR','JR','OPR']
    const NSO_ROLES = ['JT','PBM','PBT','SK','PT','LT','IWB','SBO']
    const hasAny = (roles, ref) => (_.intersection(roles, ref).length > 0)

    const l = (o) => (console.log(JSON.stringify(o, null, 2)))

    const responseObject = (status, body) => ({
        statusCode: status,
        headers: {
          'Access-Control-Allow-Origin': '*'
        },
        body: body
    })

    const authorized = (id, fbId, role) => {
        const query = {
            TableName: "hrdos_users",
            KeyConditionExpression: "#id = :id",
            ExpressionAttributeNames: {
                "#id": "id"
            },
            ExpressionAttributeValues: {
                ":id": id
            }
        }

        return docClient.query(query).promise().then((d) => {
            if (d.Count === 1 && d.Items[0].role >= role) {
                return d.Items[0]
            } else if (d.Count !== 1) {
                throw "User not found"
            } else {
                throw "Operation not allowed"
            }
        })
    }

    const getEvent = (eventId) => {
        if (!eventId) return Promise.resolve()

        const query = {
            TableName: "hrdos_events",
            KeyConditionExpression: "#id = :id",
            ExpressionAttributeNames: {
                "#id": "id"
            },
            ExpressionAttributeValues: {
                ":id": eventId
            }
        }

        return docClient.query(query).promise().then((d) => {
            if (d.Count === 1) {
                return d.Items[0]
            } else {
                throw "Event not found"
            }
        })
    }

    const getEvents = () => {
        const query = {
            TableName: "hrdos_events",
            FilterExpression: "#date > :now",
            ExpressionAttributeNames:{
                "#date": "start_date"
            },
            ExpressionAttributeValues: {
                ":now": moment().subtract(12, 'hours').format("YYYY-MM-DD HH:mm:ss")
            }
        }

        return docClient.scan(query).promise().then((d) => {
            const result = d.Items.map((item) => {
                const roster = item.roster
                const isRef = (r) => hasAny(r.roles.yes, REF_ROLES)
                const isNso = (r) => hasAny(r.roles.yes, NSO_ROLES)

                return Object.assign({}, item, {
                    refs: roster.filter((r) => (r.inEvent == 1 && isRef(r) && !isNso(r))).length,
                    nsos: roster.filter((r) => (r.inEvent == 1 && !isRef(r) && isNso(r))).length,
                    refnsos: roster.filter((r) => (r.inEvent == 1 && isRef(r) && isNso(r))).length,
                    nones: roster.filter((r) => (r.inEvent == 1 && !isRef(r) && !isNso(r))).length,
                    roster: undefined
                })

                return item
            })

            return JSON.stringify(_.sortBy(result, (r) => (r.start_date)))
        })
    }

    const getLocations = () => {
        return Promise.resolve(JSON.stringify(LOCATIONS))
    }

    const getRoster = (eventId) => {
        const query = {
            TableName: "hrdos_events",
            KeyConditionExpression: "#id = :id",
            ExpressionAttributeNames:{
                "#id": "id"
            },
            ExpressionAttributeValues: {
                ":id": eventId
            }
        }

        return docClient.query(query).promise().then((d) => ((d.Count === 1) ? JSON.stringify(d.Items[0].roster) : ""))
    }

    const getId = (fbId, fbName) => {

        const query = {
            TableName: "hrdos_users",
            IndexName: "fbid-index",
            KeyConditionExpression: "#fbid = :fbid",
            ExpressionAttributeNames: {
                "#fbid": "fbid"
            },
            ExpressionAttributeValues: {
                ":fbid": +fbId
            }
        }

        return docClient.query(query).promise().then((d) => {
            if (d.Count === 1) {
                const user = d.Items[0];

                if (user.name !== fbName) {
                    return setName(fbId, user.id, fbName);
                } else {
                    return user;
                }
            } else {
                // New registration
                const newUser = {
                    id: uuid(),
                    fbid: +fbId,
                    name: fbName,
                    role: 0,
                    location: "Helsinki"
                }

                const insert = {
                    TableName: "hrdos_users",
                    Item: newUser
                }

                return docClient.put(insert).promise().then((d) => (newUser))
            }
        })
        .then(data => JSON.stringify(data))
    }

    const setName = (fbId, userId, name) => {
        return authorized(userId, fbId, 0)
        .then((user) => {
            const newUser = Object.assign({}, user, {name: name})
            const update = {
                TableName: "hrdos_users",
                Item: newUser
            }

            return docClient.put(update).promise().then((d) => (JSON.stringify(newUser)))
        })
    }

    const setLocation = (fbId, userId, location) => {
        return authorized(userId, fbId, 0)
        .then((user) => {
            const update = {
                TableName: "hrdos_users",
                Item: Object.assign({}, user, {location: location})
            }

            return docClient.put(update).promise().then((d) => (JSON.stringify(user)))
        })
    }

    const setRoleSignup = (fbId, eventId, userId, roles, status) => (
        Promise.all([authorized(userId, fbId, 0), getEvent(eventId)])
        .then(([user, oldEvent]) => {
            const oldRosterEntry = oldEvent.roster.find((r) => (r.id === userId))
            const baseNot = (oldRosterEntry) ? _.difference(oldRosterEntry.roles.not, roles) : []
            const baseMaybe = (oldRosterEntry) ? _.difference(oldRosterEntry.roles.maybe, roles) : []
            const baseYes = (oldRosterEntry) ? _.difference(oldRosterEntry.roles.yes, roles) : []
            const newRosterEntry = {
                id: userId,
                name: user.name,
                inEvent: (oldRosterEntry) ? oldRosterEntry.inEvent : 1,
                comment: (oldRosterEntry) ? oldRosterEntry.comment : null,
                roles: {
                    not: (status === "0") ? baseNot.concat(roles) : baseNot,
                    maybe: (status === "1") ? baseMaybe.concat(roles) : baseMaybe,
                    yes: (status === "2") ? baseYes.concat(roles) : baseYes,
                }
            }

            const newEvent = Object.assign({}, oldEvent,
                {
                    roster: oldEvent.roster.filter((r) => (r.id !== userId)).concat([newRosterEntry])
                }
            )

            const update = {
                TableName: "hrdos_events",
                Item: newEvent
            }

            return docClient.put(update).promise().then((d) => (getRoster(newEvent.id)))
        })
    )

    const setSignup = (fbId, eventId, userId, status) => (
        Promise.all([authorized(userId, fbId, 0), getEvent(eventId)])
        .then(([user, oldEvent]) => {
            const oldRosterEntry = oldEvent.roster.find((r) => (r.id === userId))
            const newRosterEntry = {
                id: userId,
                name: user.name,
                inEvent: status,
                comment: (oldRosterEntry) ? oldRosterEntry.comment : null,
                roles: {
                    not: (oldRosterEntry) ? oldRosterEntry.roles.not : [],
                    maybe: (oldRosterEntry) ? oldRosterEntry.roles.maybe : [],
                    yes: (oldRosterEntry) ? oldRosterEntry.roles.yes : [],
                }
            }

            const newEvent = Object.assign({}, oldEvent,
                {
                    roster: oldEvent.roster.filter((r) => (r.id !== userId)).concat([newRosterEntry])
                }
            )

            const update = {
                TableName: "hrdos_events",
                Item: newEvent
            }

            return docClient.put(update).promise().then((d) => (getRoster(newEvent.id)))
        })
    )

    const saveComment = (fbId, eventId, userId, comment) => (
        Promise.all([authorized(userId, fbId, 0), getEvent(eventId)])
        .then(([user, oldEvent]) => {
            const oldRosterEntry = oldEvent.roster.find((r) => (r.id === userId))

            if (oldRosterEntry) {
                const newRosterEntry = Object.assign({},oldRosterEntry,
                    {
                        comment: (comment) ? comment : null
                    }
                )

                const newEvent = Object.assign({}, oldEvent,
                    {
                        roster: oldEvent.roster.filter((r) => (r.id !== userId)).concat([newRosterEntry])
                    }
                )

                const update = {
                    TableName: "hrdos_events",
                    Item: newEvent
                }

                return docClient.put(update).promise().then((d) => (getRoster(newEvent.id)))
            } else {
                return Promise.resolve("")
            }
        })
    )

    const saveEvent = (fbId, userId, name, venue, location, startTime, endTime, description, eventId)  => (
        Promise.all([authorized(userId, fbId, 1), getEvent(eventId)])
        .then(([user, oldEvent]) => {
            const newEvent = {
                id: (eventId) ? eventId : uuid(),
                location: location,
                name: name,
                venue: venue,
                start_date: startTime,
                end_date: endTime,
                creator: userId,
                description: (description) ? description : null,
                roster: (oldEvent) ? oldEvent.roster : []
            }

            const upsert = {
                TableName: "hrdos_events",
                Item: newEvent
            }

            return docClient.put(upsert).promise().then((d) => (JSON.stringify(newEvent)))
        })
    )

    const deleteEvent = (fbId, userId, eventId) => (
        Promise.all([authorized(userId, fbId, 1), getEvent(eventId)])
        .then(([user, oldEvent]) => {
            const deletion = {
                TableName: "hrdos_events",
                Key: {
                    id: eventId,
                    start_date: oldEvent.start_date
                }
            }

            return docClient.delete(deletion).promise().then((d) => (""))
        })
    )

    const param = (name) => {
        if (!event.queryStringParameters || !event.queryStringParameters[name]) {
            callback(null, responseObject(400, `Missing param ${name}`))
            throw `Missing parameter ${name}`
        } else {
            return event.queryStringParameters[name]
        }
    }

    const paramOption = (name) => (event.queryStringParameters[name])
    const paramArray = (name) => (param(name).split(","))

    const op = param("op");
    const execute = () => {
        switch (op) {
	        case "events":
		        return getEvents();
            case "roster":
            	return getRoster(param("eventId"));
            case "id":
            	return getId(param("fbId"), param("fbName"));
            case "name":
            	return setName(param("fbId"), param("userId"), param("name"));
            case "roleSignup":
            	return setRoleSignup(param("fbId"), param("eventId"), param("userId"), paramArray("role"), param("status"));
            case "signup":
            	return setSignup(param("fbId"), param("eventId"), param("userId"), param("status"));
            case "setLocation":
            	return setLocation(param("fbId"), param("userId"), param("location"));
            case "saveComment":
            	return saveComment(param("fbId"), param("eventId"), param("userId"), paramOption("comment"));
            case "saveEvent":
            	return saveEvent(param("fbId"), param("userId"), param("name"), param("venue"), param("location"), param("startTime"), param("endTime"), paramOption("description"), paramOption("eventId"));
            case "deleteEvent":
            	return deleteEvent(param("fbId"), param("userId"), param("eventId"));
            case "locations":
            	return getLocations();
        }
    }

    execute()
    .then((data) => {
        console.log(`Success in operation '${op}'`, data)
        callback(null, responseObject(200, data))
    })
    .catch((err) => {
        console.log(`Error in operation '${op}'`, err)
        callback(null, responseObject(500, JSON.stringify(err.message)))
    })

};
