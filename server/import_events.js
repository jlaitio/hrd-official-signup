'use strict';

const moment = require('moment')
const _ = require('lodash')
const uuid = require('uuid/v1')
const AWS = require('aws-sdk');
AWS.config.update({region:'eu-west-1'});
const docClient = new AWS.DynamoDB.DocumentClient()

const events = require('./migration/events.json')




Promise.all(events.map((ev) => {
    const upsert = {
        TableName: "hrdos_events",
        Item: ev
    }

    return docClient.put(upsert).promise().then((d) => (JSON.stringify(ev)))
}))
.then((d) => (console.log("done"))).catch((e) => (console.log("fail: " + e)))
